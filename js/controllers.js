angular.module('starter.controllers', [])

.controller('AppCtrl', function($stateParams,$scope,$rootScope,$ionicSideMenuDelegate,$location,$ionicHistory,$localStorage, $ionicModal, $timeout,$state,$sessionStorage,$ionicPopup,$filter,$window,APPHOST){
  $scope.menucontentopen='menu-content-open';
  $scope.menuopen='menu-open';

  // Chargement de la page
  $scope.reload=function(page){
    $window.location.href=APPHOST+"?"+DATAS.id+"#/app/"+page;
  }
 
  // Création d'une liste aléatoire
  $scope.randomList = function(list) {
      return list.sort(function() {
          return 0.5 - Math.random();
      });
  }

  console.info("Début");
  // Récupération des données du JSON
  $scope.profile=DATAS.profile
  $scope.titre=DATAS.titre;
  $scope.texte_introductif=DATAS.texte_introductif;

  // Liste des paramètres du psychotest
  var listeprofils=DATAS.profils;
  var questions=DATAS.questions;

  // Longueur des paramètres
  $scope.nbq=Object.keys(questions).length;
  $scope.nbprofils=Object.keys(listeprofils).length;

  // Paramètres dans le scope
  $localStorage.questions=questions;
  $scope.questions=questions;
  $scope.listeprofils=listeprofils;

    $scope.initialisationdesscores=function()
    {
      // Cas d'un profilage : création d'un tableau
          if($scope.profile)
          {
            $localStorage.total=[];
            angular.forEach(listeprofils,function(value,key){
              var newobject={};
              newobject['lettre']=value.code;
              newobject['score']=0;
              $localStorage.total.push(newobject);
            }); 
          }
          // Cas du calcul d'un score
          else
          {
            $localStorage.points=0;
          }
    }
  // Question et réponses de la page en cours
  if($scope.page!="portrait")
  {
    $scope.page=$stateParams.id;
  }
  if($stateParams.id)
    {
      $scope.question=$localStorage.questions[$stateParams.id];
      $scope.reponses=$scope.randomList($localStorage.questions[$stateParams.id].reponses);
      // Initialisation pour le calcul
      if($stateParams.id==1)
        { $scope.initialisationdesscores();
        }
    }


  // Passage à la question suivante
  $scope.suivant=function(){
    // Cas d'un profilage : création d'un tableau
    if($scope.profile)
          {
            angular.forEach($localStorage.total,function(value,key){
              if(value.lettre==String($scope.profil))
                {value.score=value.score+1;}
            });
          }
          // Cas du calcul d'un score
    else
      {
        console.info("storage après",$localStorage.points);
        $localStorage.points=$localStorage.points+$scope.points;
        console.info("points de la question en cours",$scope.points);
        console.info("storage après",$localStorage.points);
      }

    // Nouvelle page (test si ce n'est pas la dernière)
    if($localStorage.questions[$scope.next])
      {$state.go('app.question',{'id':$scope.next});}
    else{$scope.reload('portrait');location.reload();}
  }

  // Validation d'une réponse
  $scope.score=function(reponse){
     if($stateParams.id==1)
        { $scope.initialisationdesscores();
        }
      
      $scope.profil=reponse.profil;
      $scope.points=reponse.points
      var id=parseInt($stateParams.id);
      $scope.next=id+1;
    }
})


.controller('scoreCtrl', function($stateParams,$scope,$rootScope,$ionicSideMenuDelegate,$location,$ionicHistory,$localStorage, $ionicModal, $timeout,$state,$sessionStorage,$ionicPopup,$window,APPHOST){
 

 $scope.calculduscore=function(){

  console.info("Je finalise");
  // Cas d'un profilage : création d'un tableau
    if($scope.profile)
      {
        // On classe les scores dans l'ordre décroissant
        $localStorage.total.sort(function (a,b){ return b.score - a.score; });
        console.info("$localStorage.total 1",$localStorage.total);
        // On range les résultats et les résultats écartés
        var i=0;
        $scope.resultats=[];
        $scope.resultatsecartes=[];
        do{
          if($localStorage.total[i].score==$localStorage.total[0].score){
            $scope.resultats.push($localStorage.total[i].lettre);
          }
          else{
            $scope.resultatsecartes.push($localStorage.total[i].lettre);
          }
          i++;
        }
        while(i<$localStorage.total.length);
        console.info("$scope.resultats",$scope.resultats);
        $scope.t=""
        var sep=", "
        angular.forEach($scope.resultats,function(value,key){
          if ($scope.resultats.length-2==key){sep=" et "}
          if ($scope.resultats.length-1==key){sep="  "}
            $scope.t+=$scope.listeprofils[value].titre +sep       
        })
      }
      // Cas du calcul d'un score
     else
      {
        $scope.points=$localStorage.points
        angular.forEach(DATAS.scores,function(value,key){
          console.info($scope.points)
          if($scope.points>=value.min && $scope.points<=value.max){
            $scope.description=value.explication
            $scope.titreprofil=value.titre
            console.info(value)
          } 
        })
      }
    
  
    
  }

          
  $scope.calculduscore();
})




