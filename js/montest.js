var MONTEST = 
{
   "titre":"Test de personnalité !!!",
  "texte_introductif":"Répondez à ces quelques questions pour savoir quelle forme vous êtes.",
  "profils": {
    "1": {"code":"triangle","titre":"TRIANGLE","explication":"Vous êtes plutôt triangle"},
    "2": {"code":"carre","titre":"CARRE","explication":"Vous êtes plutôt carré"},
    "3": {"code":"rond","titre":"ROND","explication":"Vous êtes plutôt rond"}
  },
  "questions": {
    "1": {"id":1,"texte":"Combien d'angles avez-vous ?",
      "reponses":{
        "1":{"id":1,"texte":"Trois","id_question":1,"points":"triangle"},
        "2":{"id":2,"texte":"Quatre","id_question":1,"points":"carre"},
        "3":{"id":3,"texte":"je ne sais pas","id_question":1,"points":"rond"},
      
    },},
    "2": {"id":2,"texte":"Combien d'angles droits avez-vous ?",
      "reponses":{
        "1":{"id":1,"texte":"1 de temps en temps","id_question":2,"points":"triangle"},
        "2":{"id":2,"texte":"Quatre","id_question":2,"points":"carre"},
        "3":{"id":3,"texte":"aucun","id_question":2,"points":"rond"},
      
    },},
    "3": {"id":3,"texte":"Combien avez-vous de côtés ?",
      "reponses":{
        "1":{"id":1,"texte":"Trois","id_question":3,"points":"triangle"},
        "2":{"id":2,"texte":"Quatre","id_question":3,"points":"carre"},
        "3":{"id":3,"texte":"je ne sais pas","id_question":3,"points":"rond"},
      
    },}
  }
};