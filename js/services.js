//"use strict";
angular.module('services', ['ngRoute','constantes'])

.service('deleteData', function($http,HOST,$sessionStorage) {

  return {
    fromUrl: function(rest) {
      //console.info("url",rest);
      return $http.delete(HOST+rest,{"headers":{ "Authorization": "Bearer "+$sessionStorage.jwt}});  //1. this returns promise

    }
  };
})





.service('getData', function($http,HOST,$sessionStorage) {

  return {
    fromUrl: function(rest,open) {
        var header=(open)?false:true;
        console.info(rest)
        if(header){
         
       return $http.get(HOST+rest,{"headers":{ "Authorization": "Bearer "+$sessionStorage.jwt}});  //1. this returns promise
       // return $http.get(HOST+rest,{"headers":{ "content-type": "application/json"}});  //1. this returns promise

      }
      else{

        return $http.get(HOST+rest);  //1. this returns promise
      }

    }
  };
})

//Post nécessite d'initialiser header
// .config(['$httpProvider', function ($httpProvider) {
//   //Reset headers to avoid OPTIONS request (aka preflight)
//   $httpProvider.defaults.headers.common = {};
//   $httpProvider.defaults.headers.post = {};
//   $httpProvider.defaults.headers.put = {};
//   $httpProvider.defaults.headers.patch = {};
// }])

.service('postData', function($http,HOST,$sessionStorage) {
  return {
    async: function(rest,data,open) {
      var header=(open)?false:true;
     
      console.info("POST",rest);
    	 console.info(HOST+rest);
      //return $http.post(HOST+rest,data);  //1. this returns promise
      if(!header){
       //return $http.post(HOST+rest,data,{"headers":{ "Authorization": "Bearer "+$sessionStorage.jwt}});  //1. this returns promise
        return $http.post(HOST+rest,data,{"headers":{ "content-type": "application/json"}});  //1. this returns promise

      }
      else{
       return $http.post(HOST+rest,data,{"headers":{ "content-type": "application/json"}});  //1. this returns promise

      }

    }
  };
})

.service('putData', function($http,HOST,$sessionStorage) {
  return {
    async: function(rest,data) {
       console.info(HOST+rest);
        //console.info(data);
      //return $http.put(HOST+rest,data);  //1. this returns promise

       return $http.put(HOST+rest,data,{"headers":{ "Authorization": "Bearer "+$sessionStorage.jwt}});  //1. this returns promise
    }
  };
})

.service('uploadData', function($http,HOST,$sessionStorage) {
 return {
    async: function(rest,fd) { 
      console.info(HOST+rest);
return $http.post(HOST+rest, fd, {
        withCredentials: true,
        headers: {'Content-Type': undefined ,"Authorization": "Bearer "+$sessionStorage.jwt},
        transformRequest: angular.identity
    });
  }
}
})

.service('getFile', function($http,HOST) {

  return {
    fromUrl: function(rest) {
      //console.info("url",rest);
      //return $http.get(HOST+rest,{"responseType": "arraybuffer","dataType":"blob","headers":{ "Authorization": "Bearer "+$sessionStorage.jwt}});  //1. this returns promise

      return $http.get(HOST+rest,{"responseType": "arraybuffer","dataType":"blob"});  //1. this returns promise

    }
  };
})


.service('JWT', function(  ){
    
    return{

      /* Initialiation */
      init : function(){
          this.iat = null; /* connexion */
          this.nbf = null; /* accès */  
          this.exp = null; /* expiration */
          this.ens = null;
          this.ien = null;
          this.adm = null;
          /*
          this.iatString = '';
          this.nbfString = '';
          this.expString = '';
          */
          this.token = null;
          this.tokenDecode = null;
          /* heure courante */
          this.date = new Date();
          this.heureCourante = toolsService.dateObjToString( this.date);
          /* session en cours */
          if( $sessionStorage.jwt ){
            this.setJWT( $sessionStorage.jwt ).exportValues().dbug();
          }

          return this;
      },


      isExpire : function(){
          return null === this.token;
      },

      
      

      /* Expiration */
      logout  : function(){
          delete $sessionStorage.jwt;
          this.init();
          $rootScope.delai  = null;
          $location.path('/logout');
          return this;
      },

      /* Clean */
      clean : function(){
        $rootScope.message = '';
        $rootScope.niveau = 'info';
        return this;
      },

      /**
      * Decodage du JWT
      * @param jwt
      * @returns {*}
      */
      decodeToken : function( jwt ){
          var a = jwt.split(".");
          return  b64utos(a[1]); /* jsrsasign */
      },

      /**
      * Initialisation du JWT
      * @param data
      */
      setJWT  : function(data){ 
          this.token = data;
          this.claim = this.decodeToken(data);
          return this;
      },

      setExpire : function(expire){
        this.exp =new Date(expire * 1000);
        this.expString = toolsService.dateObjToString(this.exp);
      },

      /**
      * Affecte les valeurs courantes du JWT aux containeurs respectifs.
      */
      exportValues : function(){
        
          var parsedJSON = JSON.parse(this.claim);
 
          if (parsedJSON) {

            this.iss = parsedJSON.iss;
            this.data = parsedJSON.data;

            this.iat = new Date(parsedJSON.iat * 1000);
            this.nbf = new Date(parsedJSON.nbf * 1000);
            this.exp = new Date(parsedJSON.exp * 1000);

            this.iatString = toolsService.dateObjToString(this.iat);
            this.nbfString = toolsService.dateObjToString(this.nbf);
            this.expString = toolsService.dateObjToString(this.exp);

            this.ens = ( 'ens' == this.profil().role );
            this.ien = ( 'ien' == this.profil().role );
            this.adm = ( 'adm' == this.profil().role );
          }

          var beautifiedJSON = JSON.stringify( parsedJSON, null, 4);
          this.tokenDecode = (this.claim) ? beautifiedJSON : '';

          return this
      },

      profil : function(){
        if( this.data ){
          return this.data.profil;
        } else {
          return false;
        }
      },

}})
