angular.module('filters', ['ngRoute','constantes',])

.filter('type', ['FILE', function(FILE) {
  return function(input, option) {
    if (FILE[input] == option){
//      console.info("type de fichier :", input);
      return true;
    } else { return false; }
   
  };
}])

.filter('tiret', function() {
  return function(object,param) { 
   var nb = (""+object).split("");
  
  var str="";
   if(nb[1]>0){
    str="-";
   }
  if(nb[2]>0){
    str+= "-";
  }
  return " "+str+" "
   
  
   
  };
})

.filter('vide', function() {
  return function(object,param) {
    if(object){
      var vide= Object.keys(object).length === 0
    }
    else{var vide=true}
    
    if (param!="non"){

      return vide;

    
   }
   else{
     
     return !vide;
   }
  };
})

.filter('count', function() {
  return function(object,param) {

     return  Object.keys(object).length
   
    
  }
})

.filter('chemin', function(HOST,IMG) {
  return function(object,param) {

    if (!object) { return IMG+"no.png"}
   else { return object.replace("public://",IMG)}
   
   
  };
})

.filter('admin', function() {
  return function(user,param) {
   if(param==0){
      return (user)?false:true;
   }
   else{
    return (user)?true:false;
   }
    
   
   
  };
})
.filter('name', function(HOST,$localStorage) {
  return function(object,param) {
  var name=""; 
   angular.forEach($localStorage.vocs[param],function(value,key){ 
    if(value.id==object){
      console.info("OK",value.name)
   name= value.name;
    }
   })
    return name; 
  };
})
.filter('voc', function(HOST,$localStorage) {
  return function(object,param) {
  var liste=[];
  angular.forEach(object,function(elt,key){  
   angular.forEach($localStorage.vocs[param],function(value,key){ 
    if(value.id==elt[param.toLowerCase()]){
      console.info("OK",value.name)
   liste[value.id]= value.name;
    }
   })
 })
  text=""
   angular.forEach(liste,function(name,key){ 
text+=name+" "
   })
    return text; 
  };
})
// .filter('logo', function(HOST) {
//   return function(object,param) {
   
//     return HOST.replace("web/","")+object.replace("public://","thumbnail/");
   
   
//   };
// })

.filter('ordre', function() {
  return function(object,param) {
    
   
   arr=Object.keys(object).map(function (key) { return object[key]; });
  
   return arr
  };
})


.filter('label', function($sessionStorage) {
  return function(object,param) {
    var labels=[]
    angular.forEach(object , function(champ, key){
    labels[key-1]=champ.titre;
    })
    return labels;
   
  };
})

.filter('palier', function($sessionStorage,COLOR) {
  // Dans FILTER, 
  //1er paramètre = palier en cours
  //2eme paramètre = ce qu'on veut connaître
  return function(object,param,parambis) {
    // DIFFERENTES COULEURS ECRITES EN DUR
    // TODO : CSS
   
    // Dans chaque champ, on complète un tableau donnant 1 ou 0 par palier acquis ou non
    angular.forEach(object.palierseries , function(champ, key){
      if(key,champ[0]/object.total[key+1]*100>=75)
      {
          object.paliersvalides[key+1]=1;
      }
    })
    // Si le palier est acquis, on envoie les paramètres de couleur
    if(object.paliersvalides[param]==1)
      {return COLOR[param][parambis];}
    // Sinon on renvoie les paramètres de couleur par défaut
    else{return COLOR[0][parambis];}
  }
})

.filter('pourcent', function($sessionStorage) {
  return function(champ,param) {
  var valide=0;
  var encours=0;
   var out=0;
var pourcent=[]
    angular.forEach(champ.composantes , function(composante, key){
      if(composante.items){


       angular.forEach(composante.items , function(item, key){
          if($sessionStorage.items[item.id] && $sessionStorage.items[item.id].checked ){
            valide++;
          }
          else if($sessionStorage.items[item.id] && $sessionStorage.items[item.id].encours){
            encours++;
          }
          else {
            out++;
          }
       })
     }
    })
    var pourcent=[{'nb':valide,'color':'green'},{'nb':encours,'color':'orange'},{'nb':out,'color':'green'}]
    // var html="";
    // angular.forEach(pourcent,function(value,key){
    //   html+="<div style='color:"+value.color+" ;height:10px;width:"+value.nb+"px'></div>"
    // })
    return  pourcent;
  };
})


.filter('htmlToPlaintext', function() {
    return function(text) {
      var newtext=text ? String(text).replace(/<[^>]+>/gm, '') : '';
      return  newtext;
    };
  }
)
