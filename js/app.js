// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','services','directives','filters','ngStorage'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})



.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider


.state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/question/menu.html',
     controller: 'AppCtrl'
  })
.state('app.question', {
    url: '/question/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/question.html',
       controller: 'AppCtrl'
      }
    }
  })
.state('app.portrait', {
    url: '/portrait',
    views: {
      'menuContent': {
        templateUrl: 'templates/portrait.html',
       controller: 'scoreCtrl'
      }
    }
  })
.state('app.accueil', {
    url: '/accueil',
    views: {
      'menuContent': {
        templateUrl: 'templates/accueil.html',
       controller: 'AppCtrl'
      }
    }
  })


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/accueil');

})
.config( 
    
    function( $ionicConfigProvider )
    {   
       var isMobile=ionic.Platform.isIOS() || ionic.Platform.isAndroid();
      $ionicConfigProvider.scrolling.jsScrolling(isMobile)
        // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
    }

    
)
.config(function ($httpProvider) {
  $httpProvider.defaults.headers.common = {};
  $httpProvider.defaults.headers.post = {};
  $httpProvider.defaults.headers.put = {};
  $httpProvider.defaults.headers.patch = {};
});




