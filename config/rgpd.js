DATAS={
  id:"rgpd",
   "titre":"Etes-vous RGPD compatible ?",
  "texte_introductif":"Le règlement général sur la protection des données (RGPD) est entré en application le 25 mai 2018. Avez-vous bien compris les enjeux du RGPD pour votre établissement ?  Savez-vous ce que la loi vous impose en matière de protection des données ?  ",
  "profile":false,
  "scores":[
    {'min':0,'max':5,'titre':'nul','explication':'Il ne faut pas se mentir : le RGPD et le numérique en général, ce n\'est pas votre problème. Mais il va quand même falloir s\'y mettre.'},
    {'min':6,'max':10,'titre':'top','explication':'Nous vous conseillons une formation...'},
    {'min':11,'max':20,'titre':'top','explication':'Bravo ! Avec vous les données de la communauté éducative sont bien protégées.'}
  ],
  "profils": {
    "triangle": {"code":"triangle","titre":"TRIANGLE","explication":"Vous êtes plutôt triangle"},
    "carre": {"code":"carre","titre":"CARRE","explication":"Vous êtes plutôt carré"},
    "rond": {"code":"rond","titre":"ROND","explication":"Vous êtes plutôt rond"},
    "losange": {"code":"losange","titre":"LOSANGE","explication":"Vous êtes plutôt losange"}

  },
  "questions": {
    "1": {"id":1,"texte":" Dans le cadre du RGPD, qui est responsable du traitement des données à caractère personnel dans les collèges et lycées ?",
      "reponses":[
        {"id":1,"texte":"Le chef d'établissement.","id_question":1,"profil":"triangle","points":5},
        {"id":2,"texte":"Le référent numérique.","id_question":1,"profil":"carre","points":0},
        {"id":3,"texte":"Chaque enseignant qui manipule les données personnelles de ces élèves.","id_question":0,"profil":"rond","points":1},
        {"id":4,"texte":"Le recteur","id_question":1,"profil":"losange","points":0}
      
    ],},
    "2": {"id":2,"texte":"Savez-vous qui est le DPD ?",
      "reponses":[
        {"id":1,"texte":"Le Délégué à la Protection des Données veille à la protection des données dans l'établissement scolaire : il contrôle, sensibilise, informe.","id_question":2,"profil":"triangle","points":5},
       {"id":2,"texte":"Le Délégué à la Protection des Données est le référent numérique, il assiste le chef d'établissement dans sa  mission de prote ction des données. ","id_question":2,"profil":"carre","points":0},
        {"id":3,"texte":"Le Délégué à la Propagation des Données a pour mission de s'assurer que les flux d'information sont efficients dans l'établissement scolaire.","id_question":2,"profil":"rond","points":0},
        {"id":4,"texte":"Je ne sais pas et je ne veux pas le savoir.","id_question":1,"profil":"losange","points":0}
      
    ],},
    "3": {"id":3,"texte":"Qui désigne le DPD dans un collège ou un lycée ?",
      "reponses":[
        {"id":1,"texte":"Le chef d'établissement, mais il peut s'en remettre au DPD académique. ","id_question":3,"profil":"triangle","points":5},
        {"id":2,"texte":"Le recteur : la tradition veut qu'il s'en remette au doyen des profs de maths de chaque établissement.","id_question":3,"profil":"carre","points":0},
        {"id":3,"texte":"Le chef d'établissement, sur proposition du conseil d'administration.","id_question":3,"profil":"rond","points":0},
        {"id":4,"texte":"Le chef d'établissement, en procédant à un tirage au sort.","id_question":1,"profil":"losange","points":0}
      
    ],},
    "4": {"id":3,"texte":"Le chef d'établissement doit veiller à ce que soit mis à jour un registre des activités de traitement. ",
      "reponses":[
        {"id":1,"texte":"Tout traitement de données à caractère personnel (absences, notes, accès à un service pédagogique nominatif,...) doit faire l’objet d’une inscription sur le registre de l’établissement.","id_question":3,"profil":"triangle","points":5},
        {"id":2,"texte":"Les enseignants doivent y inscrire le traitement qu'ils réservent aux données personnelles des élèves (notes,appréciations,...) et combien de temps ils les conservent.","id_question":3,"profil":"carre","points":0},
        {"id":3,"texte":"Ce registre peut être en ligne mais il doit à son tour faire l'objet d'une déclaration CNIL ","id_question":3,"profil":"rond","points":0},
        {"id":4,"texte":"Le ministère conseille d'utiliser google drive pour que chaque membre de la communauté éducative puisse le compléter simplement.","id_question":1,"profil":"losange","points":0}
      
    ],}
  }
};