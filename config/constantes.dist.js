angular.module('constantes', [])

.constant("HOST","https://rest.canope-creteil.fr/premat/rest/web/")
.constant("HOSTAPP","https://rest.canope-creteil.fr/app/")
.constant("IMG","https://rest.canope-creteil.fr/premat/rest/img/")
.constant("COLOR",[
	{'couleur_fond':'#cccccc','couleur_police':'white','couleur_opacite':0.5}, // gris clair

/* Coloration des paliers : bleu, violet, anthracite */
    {'couleur_fond':'#3498db','couleur_police':'white','couleur_opacite':1}, // bleu
    {'couleur_fond':'#9b59b6','couleur_police':'white','couleur_opacite':1}, // violet
    {'couleur_fond':'#34495e','couleur_police':'white','couleur_opacite':1}, // anthracite
    {'couleur_fond':'#34bcdb','couleur_police':'white','couleur_opacite':1},
    {'couleur_fond':'#cd5cf9','couleur_police':'white','couleur_opacite':1},
    {'couleur_fond':'#7998b7','couleur_police':'white','couleur_opacite':1}//, 

/* Coloration des paliers graduée : bleu clair, bleu, indigo */
/*  {'couleur_fond':'#039BE5','couleur_police':'white','couleur_opacite':1}, // light blue 600
    {'couleur_fond':'#1565C0','couleur_police':'white','couleur_opacite':1}, // blue 800
    {'couleur_fond':'#3949AB','couleur_police':'white','couleur_opacite':1}, // indigo 600
    {'couleur_fond':'#B3E5FC','couleur_police':'black','couleur_opacite':1}, // light blue 100
    {'couleur_fond':'#90CAF9','couleur_police':'black','couleur_opacite':1}, // blue 200
    {'couleur_fond':'#9FA8DA','couleur_police':'black','couleur_opacite':1}, // indigo 200 */
])
.constant("MAXQUANTITE",10)