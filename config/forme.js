DATAS= 
{
  id:"forme",
   "titre":"Test de personnalité !!!",
  "texte_introductif":"Répondez à ces quelques questions pour savoir quelle forme vous êtes.",
  "profile":true,
  "profils": {
    "triangle": {"code":"triangle","titre":"TRIANGLE","explication":"Vous êtes plutôt triangle"},
    "carre": {"code":"carre","titre":"CARRE","explication":"Vous êtes plutôt carré"},
    "rond": {"code":"rond","titre":"ROND","explication":"Vous êtes plutôt rond"}
  },
  "questions": {
    "1": {"id":1,"texte":"Combien d'angles avez-vous ?",
      "reponses":[
        {"id":1,"texte":"Trois","id_question":1,"profil":"triangle"},
        {"id":2,"texte":"Quatre","id_question":1,"profil":"carre"},
        {"id":3,"texte":"je ne sais pas","id_question":1,"profil":"rond"},
    ],},
    "2": {"id":2,"texte":"Combien d'angles droits avez-vous ?",
      "reponses":[
        {"id":1,"texte":"1 de temps en temps","id_question":2,"profil":"triangle"},
        {"id":2,"texte":"Quatre","id_question":2,"profil":"carre"},
        {"id":3,"texte":"aucun","id_question":2,"profil":"rond"},
      
    ],},
    "3": {"id":3,"texte":"Combien avez-vous de côtés ?",
      "reponses":[
        {"id":1,"texte":"Trois","id_question":3,"profil":"triangle"},
        {"id":2,"texte":"Quatre","id_question":3,"profil":"carre"},
        {"id":3,"texte":"je ne sais pas","id_question":3,"profil":"rond"},
      
    ],}
}
};