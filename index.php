<?php
$datas=str_replace ("/","",$_SERVER['QUERY_STRING']);
$datas=$datas.".js";

?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    <title></title>
    <link href="lib/ionic/css/ionic.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- IF using Sass (run gulp sass first), then uncomment below and remove the CSS includes above
    <link href="css/ionic.app.css" rel="stylesheet">
    -->
    <script src="lib/jquery.min.js"></script>
    <script src="lib/tinymce/tinymce.min.js"></script>
    <script src="lib/ionic/js/ionic.bundle.js"></script>
    <!-- cordova script (this will be a 404 during development)
    <script src="cordova.js"></script>
     -->
    <!-- your app's js -->
    <script src="js/app.js"></script>
    <script src="config/constantes.js"></script>
    <script src="config/<?php echo $datas;?>"></script>
    <script src="lib/ngstorage/ngStorage.min.js"></script>
    <script src="lib/angular-route/angular-route.js"></script>
    <script src="lib/vendor/jsrsasign-4.1.4-all-min.js"></script>
    <script src="js/controllers.js"></script>
    <script src="js/services.js"></script>
    <script src="js/directives.js"></script>
    <script src="js/filters.js"></script>
    
  </head>
  <body ng-app="starter">
    <ion-nav-view></ion-nav-view>
  </body>
</html>